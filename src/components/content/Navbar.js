import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignLeft, faFilter } from "@fortawesome/free-solid-svg-icons";
import { Navbar, Button } from "react-bootstrap";

class NavBar extends React.Component {
  render() {
    return (
      <Navbar
        bg="light"
        className="navbar shadow-sm p-3 mb-3 bg-light rounded"
        expand
      >
        <Button className="btn-light" onClick={this.props.toggle}>
          <FontAwesomeIcon icon={faAlignLeft} />
        </Button>

        <div className="mx-auto mt-2">
          <h5 className="font-weight-bold">Awards</h5>
        </div>

        <Button className="btn-light" onClick={this.props.toggle}>
          <FontAwesomeIcon icon={faFilter} />
        </Button>
      </Navbar>
    );
  }
}

export default NavBar;
