import React from "react";
import axios from "axios";
import classNames from "classnames";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import NavBar from "./Navbar";
import LoyaltyCard from "../card/LoyaltyCard";
class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = { loyaltyArray: [], param: props.param };
  }

  componentDidMount() {
    var urlAPI = "http://localhost:3002/awards-service/list?page=1";

    if (this.state.param.p) urlAPI = urlAPI + "&size=" + this.state.param.p;
    if (this.state.param.min) urlAPI = urlAPI + "&min=" + this.state.param.min;
    if (this.state.param.max) urlAPI = urlAPI + "&max=" + this.state.param.max;
    if (this.state.param.type)
      urlAPI = urlAPI + "&type=" + this.state.param.type;

    axios
      .get(urlAPI)
      .then((res) => {
        this.setState({ loyaltyArray: res.data.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  dataLoyalty() {
    return this.state.loyaltyArray.map((data, i) => {
      return (
        <LoyaltyCard
          type={data.type}
          img={data.image}
          name={data.name}
          poin={data.poin}
        />
      );
    });
  }

  render() {
    return (
      <Container
        fluid
        className={classNames("content", { "is-open": this.props.isOpen })}
      >
        <NavBar toggle={this.props.toggle} />

        <Row>
          <Col sm={12}>
            <Form action="/" method="get">
              <Col sm={4}>
                <Form.Group className="mb-3" controlId="min">
                  <Form.Control
                    name="min"
                    type="number"
                    placeholder="Minimal Poin"
                  />
                </Form.Group>
              </Col>
              <Col sm={6}>
                <Form.Group className="mb-3" controlId="max">
                  <Form.Control
                    name="max"
                    type="number"
                    placeholder="Maximal Poin"
                  />
                </Form.Group>
              </Col>
              <Col sm={12}>
                <div className="mb-3">
                  <Form.Check
                    type={"radio"}
                    id={"check-voucher"}
                    label={"Voucher"}
                    value={"voucher"}
                    name={"type"}
                  />
                </div>
                <div className="mb-3">
                  <Form.Check
                    type={"radio"}
                    id={"check-giftcard"}
                    label={"Giftcard"}
                    value={"giftcard"}
                    name={"type"}
                  />
                </div>
                <div className="mb-3">
                  <Form.Check
                    type={"radio"}
                    id={"check-product"}
                    label={"Product"}
                    value={"product"}
                    name={"type"}
                  />
                </div>
              </Col>

              <Col sm={12}>
                <Button className="btn btn-block btn-primary" type="submit">
                  Submit
                </Button>
              </Col>
            </Form>
          </Col>
        </Row>

        {this.dataLoyalty()}
      </Container>
    );
  }
}

export default Content;
