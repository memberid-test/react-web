import React from "react";
import { Row, Card, Col } from "react-bootstrap";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function LoyaltyCard(props) {
  const LogoImage = props.img
    ? props.img
    : "https://cdn.techinasia.com/data/images/iSrhOKmjthdKkunqBJc6gUhegLnpkz4P5uznPIEz.jpeg";

  return (
    <>
      <Row className="mt-3">
        <Col sm={12}>
          <Card
            className="rounded border-0 shadow-sm text-white"
            style={{
              width: "100%",
              backgroundImage: `url(${LogoImage})`,
              backgroundPosition: "center",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              height: "200px",
            }}
          >
            <Card.Body>
              <Card.Title>
                <span
                  className={`rounded badge  p-2 rounded ${
                    props.type === "voucher"
                      ? "badge-primary"
                      : props.type === "giftcard"
                      ? "badge-warning"
                      : props.type === "product"
                      ? "badge-success"
                      : "badge-danger"
                  }`}
                >
                  {capitalizeFirstLetter(props.type)}
                </span>
              </Card.Title>
              <Card.Title
                style={{
                  fontWeight: 700,
                  color: "black",
                  marginTop: "110px",
                }}
              >
                {numberWithCommas(props.poin) + " poin"}
              </Card.Title>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col className="mt-2" sm={12}>
          <h5 className="font-weight-bold">{props.name}</h5>
        </Col>
      </Row>
    </>
  );
}

export default LoyaltyCard;
